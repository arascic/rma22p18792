package ba.etf.rma22.projekat.data.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.Grupa

@Dao
interface AnketaDAO {
    @Query("delete from Anketa")
    fun drop()

    @Query("select * from Anketa where id <= :offset * 5 and id > (:offset - 1) * 5")
    fun getAll(offset: Int): List<Anketa>

    @Query("select count(*) from Anketa where id <= :offset * 5 and id > (:offset - 1) * 5")
    fun getCount(offset: Int): Int

    @Query("select * from Anketa")
    fun getAll(): List<Anketa>

    @Query("select count(*) from Anketa")
    fun getCount(): Int

    @Insert(onConflict = REPLACE)
    fun insert(anketa: Anketa)

    @Query("select * from Anketa where id = :id")
    fun getByID(id: Int): Anketa

    @Query("select count(*) from Anketa where id = :id")
    fun hasID(id: Int): Int

    @Query("select * from Anketa where grupaFK = :gid")
    fun getAnketeGrupe(gid: Int): List<Anketa>

    @Query("select count(*) from Anketa where grupaFK = :gid")
    fun hasAnketeGrupe(gid: Int): Int
}