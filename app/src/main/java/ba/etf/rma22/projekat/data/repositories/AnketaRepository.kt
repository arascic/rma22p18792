package ba.etf.rma22.projekat.data.repositories

import android.util.Log
import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.DB
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.Korisnik
import ba.etf.rma22.projekat.retrofit.ApiAdapter
import ba.etf.rma22.projekat.retrofit.Connection
import ba.etf.rma22.projekat.viewmodel.AnketaViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.time.Instant.now
import java.util.*

class AnketaRepository {
    companion object {
        private var ankete: List<Anketa> = mutableListOf()

        val dao = DB.getInstance()!!.anketaDAO()

        fun MyDate(y: Int, m: Int, d: Int): Date {
            val cal: Calendar = Calendar.getInstance()
            cal.set(1900 + y, m, d)
            return cal.time;
        }

        suspend fun pripremiAnkete(): ArrayList<Anketa> {
            var spremneAnkete: ArrayList<Anketa> = arrayListOf()
            ankete = getAll()

            while(ankete.size == 0) {
                Log.d("spin", "spin")
            }

            var xpectedSize = ankete.size

            ankete.forEach { anketa -> AnketaViewModel.mapping({
                    out ->

                val temp = anketa.copy()

                out.forEach {
                    if(!temp.nazivIstrazivanja.equals(""))
                        temp.nazivIstrazivanja += ", " + it.naziv
                    else
                        temp.nazivIstrazivanja = it.naziv
                }

                spremneAnkete.add(temp)
            }, {}, anketa.id) }

            val now = now()
            while(spremneAnkete.size != xpectedSize) {
                Log.d("spin", "spin")
                if((now().toEpochMilli() - now.toEpochMilli()) > 3000)
                    break
            }

            spremneAnkete.forEach {
                if(it != null) {
                    it.fixUp()
                    it.computeAnketaTaken()
                    while (it.busy!!) {
                        Log.d("spin666", "spin")
                    }
                }
            }

            ankete = spremneAnkete

            return spremneAnkete
        }

        suspend fun getById(id: Int): Anketa? {
            return withContext(Dispatchers.IO) {
                if(dao.hasID(id) != 0) return@withContext dao.getByID(id)

                val response = ApiAdapter.retrofit.getById(id)
                val responseBody = response.body()

                if(responseBody!!.naziv != null) dao.insert(responseBody!!)
                return@withContext responseBody
            }
        }

        suspend fun getAnketa(name: String, istrazivanje: String): Anketa {
            return getAll().filter {
                it.naziv.equals(name) && it.nazivIstrazivanja.equals(istrazivanje)
            }[0]
        }

        suspend fun getUpisane(): List<Anketa> {
            return withContext(Dispatchers.IO) {
                val grupe = IstrazivanjeIGrupaRepository.getUpisaneGrupe()
                val rezultat: ArrayList<Anketa> = ArrayList()

                for(g in grupe!!) {
                    val anketeGrupe = getAnketeIzGrupe(g.id)
                    anketeGrupe?.forEach { rezultat.add(it) }
                }

                return@withContext rezultat
            }
        }

        suspend fun getAnketeIzGrupe(id: Int): List<Anketa>? {
            return withContext(Dispatchers.IO) {
                if(Connection.isInternetAvailable(MainActivity.context) == false) {
                    val out = mutableListOf<Anketa>()
                    DB.getInstance()!!.anketaGrupaDAO().getAnketeGrupe(id).forEach {
                        out.add(DB.getInstance()!!.anketaDAO().getByID(it))
                    }
                    return@withContext out
                }

                val response = ApiAdapter.retrofit.getAnketaIzGrupe(id)
                val responseBody = response.body()
                responseBody!!.forEach {
                    it.gid = id
                    dao.insert(it)
                }
                return@withContext responseBody
            }
        }

        fun sortiraj() {
            ankete = ankete.sortedWith { lhs, rhs ->
                if (lhs.datumPocetkaD!! < rhs.datumPocetkaD) -1 else 1
            }
        }

        fun getMyAnkete(): List<Anketa> {
            var upisaneAnkete = mutableListOf<Anketa>()
            var spin: Int = Korisnik.getUpisani().size

            Korisnik.getUpisani().forEach {
                AnketaViewModel.getAnketeIzGrupe({
                    upisaneAnkete.addAll(it)
                    spin -= 1
                }, {}, it.id)
            }

            val now = now()
            while(spin != 0) {
                Log.d("spinning", "im spinning")
                if((now().toEpochMilli() - now.toEpochMilli()) > 1500)
                    break
            }

            return upisaneAnkete.toSet().toMutableList()
        }

        suspend fun getAll(offset: Int = 0): List<Anketa> {
            val out: MutableList<Anketa> = mutableListOf()
            lateinit var response: List<Anketa>
            val db = DB.getInstance()!!

            return withContext(Dispatchers.IO) {
                if(Connection.isInternetAvailable(MainActivity.context) == false) {
                    if(offset == 0) return@withContext DB.getInstance()!!.anketaDAO().getAll()
                    else return@withContext DB.getInstance()!!.anketaDAO().getAll(offset)
                }

                if(offset != 0) {
                    val responseBody = ApiAdapter.retrofit.getAll(offset).body()!!
                    responseBody.forEach { db.anketaDAO().insert(it) }
                    return@withContext responseBody
                }

                // if(ankete.isNotEmpty()) return@withContext ankete

                var i = 1
                while(
                    (ApiAdapter.retrofit.getAll(i).body()!!.also { response = it })
                        .isNotEmpty()
                ) {
                    out.addAll(response)
                    i++
                }

                out.forEach { dao.insert(it) }

                return@withContext out
            }
        }

        fun getDone(): List<Anketa> {
            // plava
            val gotovi = mutableListOf<Anketa>()
            for(k in ankete) if(k != null && k.datumRada != null) gotovi.add(k)
            return gotovi
        }

        fun getFuture(): List<Anketa> {
            val out = mutableListOf<Anketa>()
            val now: Date = Calendar.getInstance().time
            for(k in ankete)
                if(k != null && (k.datumKrajaD == null || (k.datumKrajaD)!! > now)
                    && k.datumRada == null)
                        out.add(k)
            return out
        }

        fun getNotTaken(): List<Anketa> {
            val out = mutableListOf<Anketa>()
            for(k in ankete) if(k != null && k.datumRada == null && k in getAllPast()) out.add(k)
            return out
        }

        fun getActiveNotDone(): List<Anketa> {
            val out = mutableListOf<Anketa>()
            val done: List<Anketa> = getDone()
            val now: Date = Calendar.getInstance().time

            for(k in ankete)
                if(k != null && (k.datumKrajaD == null || k.datumKrajaD!! > now)
                    && k.datumPocetkaD!! < now && !(k in done))
                        out.add(k)

            return out
        }

        fun getNextToBeActive(): List<Anketa> {
            val out = mutableListOf<Anketa>()
            val now: Date = Calendar.getInstance().time
            for(k in ankete) if(k != null && now < k.datumPocetkaD) out.add(k)
            return out
        }

        fun getPastNotDone(): List<Anketa> {
            // crvena
            val out = mutableListOf<Anketa>()
            val notTaken: List<Anketa> = getNotTaken()
            val now: Date = Calendar.getInstance().time
            for(k in notTaken) if(k != null && k.datumKrajaD != null && k.datumKrajaD!! < now) out.add(k)
            return out
        }

        fun getSize(): Int {
            return ankete.size
        }

        fun getAllPast(): List<Anketa> {
            val out = mutableListOf<Anketa>()
            val now = Calendar.getInstance().time
            for(k in ankete) if(k != null && k.datumKrajaD != null && k.datumKrajaD!! < now && k.datumRada == null)
                out.add(k)
            return out
        }
    }

    var companion = Companion
}