package ba.etf.rma22.projekat.data.models

import ba.etf.rma22.projekat.data.repositories.AnketaRepository

enum class AnketaState {
    NOT_ACTIVE, EXPIRED_NOT_DONE, ACTIVE_NOT_DONE, DONE;

    companion object {
        fun getState(anketa: Anketa): AnketaState {
            if(anketa in AnketaRepository.getActiveNotDone())
                return ACTIVE_NOT_DONE
            else if(anketa.datumRada != null)
                return DONE
            else if(anketa in AnketaRepository.getPastNotDone())
                return EXPIRED_NOT_DONE
            else
                return NOT_ACTIVE
        }
    }
}