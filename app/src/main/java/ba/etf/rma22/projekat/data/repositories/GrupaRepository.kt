package ba.etf.rma22.projekat.data.repositories

import android.util.Log
import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.DB
import ba.etf.rma22.projekat.data.models.AnketaGrupa
import ba.etf.rma22.projekat.data.models.Grupa
import ba.etf.rma22.projekat.retrofit.ApiAdapter
import ba.etf.rma22.projekat.retrofit.Connection
import kotlinx.coroutines.*

class GrupaRepository {
    companion object {
        var grupe: MutableList<Grupa> = mutableListOf()

        fun getGroupsByIstrazivanje(nazivPredmeta: String): List<Grupa> {
            return grupe.filter { it.nazivIstraživanja.equals(nazivPredmeta) }
        }

        fun getGroupByName(name: String): Grupa {
            return (grupe.filter { it.naziv.equals(name) })[0]
        }

        fun getAll(): List<Grupa> { return grupe }

        suspend fun mapping(idAnketa: Int): List<Grupa>? {
            return withContext(Dispatchers.IO) {
                if(Connection.isInternetAvailable(MainActivity.context) == false) {
                    val out = mutableListOf<Grupa>()
                    val gdao = DB.getInstance()!!.grupaDAO()
                    DB.getInstance()!!.anketaGrupaDAO().getGrupeAnkete(idAnketa).forEach {
                        out.add(gdao.getObjByID(it))
                    }

                    return@withContext out
                }

                val response = ApiAdapter.retrofit.mapping(idAnketa)
                val responseBody = response.body()

                val dao = DB.getInstance()!!.anketaGrupaDAO()
                responseBody!!.forEach {
                    val ag = AnketaGrupa(it.id, idAnketa)
                    dao.insert(ag)
                }

                return@withContext responseBody
            }
        }
    }
}