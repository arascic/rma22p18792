package ba.etf.rma22.projekat.data.repositories

import android.util.Log
import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.DB
import ba.etf.rma22.projekat.retrofit.ApiAdapter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AccountRepository {
    companion object {
        var acHash: String = "695d65da-9d52-40d6-bcd8-f44a29805769"

        suspend fun postaviHash(payload: String): Boolean {
            return withContext(Dispatchers.IO) {
                acHash = payload;
                pobrisi().accountDAO().postaviHash(payload)
                return@withContext true
            }
        }

        fun getHash(): String { return acHash }

        fun pobrisi(): DB {
            val db = DB.getInstance(MainActivity.context)
            db.dropUser()
            return db
        }

        suspend fun dodajAccount(payload: String): Boolean {
            return withContext(Dispatchers.IO) {
                val acc = ApiAdapter.retrofit.getAccount(payload).body()
                pobrisi()
                DB.getInstance()!!.accountDAO().insertAccount(acc!!)
                return@withContext true
            }
        }

        suspend fun deleteAccData(hash: String): Boolean {
            return withContext(Dispatchers.IO) {
                ApiAdapter.retrofit.deleteAll(hash)
                return@withContext true
            }
        }
    }
}