package ba.etf.rma22.projekat.retrofit

import ba.etf.rma22.projekat.data.models.*
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*

interface API {
    @GET("/anketa/{id}/pitanja")
    suspend fun getPitanja(@Path("id") Id: Int): Response<List<Pitanje>>

    @POST("/student/{id}/anketa/{kid}")
    suspend fun zapocniAnketu(@Path("id") hash: String, @Path("kid") kid: Int): Response<AnketaTaken>

    @GET("/student/{id}/anketataken")
    suspend fun getPoceteAnkete(@Path("id") id: String): Response<List<AnketaTaken>>

    @GET("/student/{id}/anketataken/{ktid}/odgovori")
    suspend fun getOdgovoriAnketa(@Path("id") id: String, @Path("ktid") kid: Int): Response<List<Odgovor>>

    @POST("/student/{id}/anketataken/{ktid}/odgovor")
    // Odgovor umjesto ResponseBody
    suspend fun postaviOdgovorAnketa(
        @Path("id") hash: String, @Path("ktid") id: Int,
        @Body odg: OdgovorRequest
    ): Response<Odgovor>

    @GET("/anketa")
    suspend fun getAll(@Query("offset") offset: Int): Response<List<Anketa>>

    @GET("/anketa/{id}")
    suspend fun getById(@Path("id") id: Int): Response<Anketa>

    @GET("/student/{id}/grupa")
    suspend fun getUpisaneGrupe(@Path("id") hash: String): Response<List<Grupa>>

    @GET("/grupa/{id}/ankete")
    suspend fun getAnketaIzGrupe(@Path("id") id: Int): Response<List<Anketa>>

    @GET("/istrazivanje")
    suspend fun getIstrazivanja(@Query("offset") offset: Int): Response<List<Istrazivanje>>

    @GET("/grupa")
    suspend fun getGrupe(): Response<List<Grupa>>

    @POST("/grupa/{gid}/student/{id}")
    suspend fun upisiUGrupu(@Path("gid") idGrupa: Int, @Path("id") hash: String): Response<UpisResponse>

    @GET("/anketa/{id}/grupa")
    suspend fun mapping(@Path("id") idKviza: Int): Response<List<Grupa>>

    @GET("student/{id}")
    suspend fun getAccount(@Path("id") hash: String): Response<Account>

    @DELETE("/student/{id}/upisugrupeipokusaji")
    suspend fun deleteAll(@Path("id") hash: String)
}