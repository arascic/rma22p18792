package ba.etf.rma22.projekat.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.AnketaState
import ba.etf.rma22.projekat.data.repositories.AnketaRepository
import ba.etf.rma22.projekat.viewmodel.AnketaViewModel
import ba.etf.rma22.projekat.viewmodel.ViewPagerAdapter
import ba.etf.rma22.projekat.R
import ba.etf.rma22.projekat.retrofit.Connection
import java.time.LocalDate.now

class FragmentPredaj(val anketa: Anketa) : Fragment() {

    private lateinit var topView: View
    private var progres: TextView? = null
    private lateinit var predajBtn: Button

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        topView = inflater.inflate(R.layout.fragment_predaj, container, false)

        progres = topView.findViewById(R.id.progresTekst)
        predajBtn = topView.findViewById(R.id.dugmePredaj)

        predajBtn.setOnClickListener { onPredaj(it) }

        if(
            !Connection.isInternetAvailable(MainActivity.context)
            || AnketaState.getState(anketa) != AnketaState.ACTIVE_NOT_DONE
        ) {
            predajBtn.isEnabled = false
        }

        refreshProgress()

        return topView
    }

    fun refreshProgress() {
        if(progres == null) return
        val realProg = anketa.progres
        var progVal : Int = anketa.progres.toInt().div(20)

        if((progVal + 1) * 20 - realProg < realProg - progVal * 20)
            progVal = (progVal + 1) * 20
        else
            progVal = progVal * 20

        progres!!.text = "progres: $progVal%"
    }

    fun onPredaj(view: View) {
        val now = now()
        anketa.datumRada = AnketaRepository.MyDate(
            now.year - 1900,
            now.monthValue - 1,
            now.dayOfMonth
        )

        FragmentAnkete.urađeneAnkete.add(anketa)

        val adapter = ((requireActivity() as MainActivity).viewPager.adapter
                as ViewPagerAdapter)

        if(anketa.responses!!.isNotEmpty()) {
            AnketaViewModel.posaljiOdgovore(
                {
                    val newFrags = mutableListOf<Fragment>()
                    newFrags.add(0, FragmentAnkete())
                    newFrags.add(1, FragmentPoruka.newInstance(anketa))

                    adapter.fragments = newFrags
                    MainActivity.frags = newFrags
                    (requireActivity() as MainActivity).runOnUiThread {
                        (requireActivity() as MainActivity).viewPager.currentItem = 1
                        adapter.notifyDataSetChanged()
                    }

                    anketa.anketaTaken = FragmentPitanje.anketaTaken.id
                },
                {},
                FragmentPitanje.anketaTaken.id,
                anketa.responses!!
            )
        }
        else {
            val newFrags = mutableListOf<Fragment>()
            newFrags.add(0, FragmentAnkete())
            newFrags.add(1, FragmentPoruka.newInstance(anketa))

            adapter.fragments = newFrags
            MainActivity.frags = newFrags
            (requireActivity() as MainActivity).viewPager.currentItem = 1
            (requireActivity() as MainActivity).runOnUiThread {
                adapter.notifyDataSetChanged()
            }

            anketa.anketaTaken = FragmentPitanje.anketaTaken.id
        }
    }
}