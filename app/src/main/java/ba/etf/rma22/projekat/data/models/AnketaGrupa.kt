package ba.etf.rma22.projekat.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(primaryKeys = ["grupaID", "anketaID"])
data class AnketaGrupa(
    @ColumnInfo(name = "grupaID") val gid: Int,
    @ColumnInfo(name = "anketaID") val aid: Int
)