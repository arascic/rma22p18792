package ba.etf.rma22.projekat.data.repositories

import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.DB
import ba.etf.rma22.projekat.data.models.*
import ba.etf.rma22.projekat.retrofit.ApiAdapter
import ba.etf.rma22.projekat.retrofit.Connection
import ba.etf.rma22.projekat.viewmodel.AnketaViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class OdgovorRepository {
    companion object {
        suspend fun getOdgovoriAnketa(idAnkete: Int): List<Odgovor>? {
            return withContext(Dispatchers.IO) {
                val db = DB.getInstance()!!

                val pocete = TakeAnketaRepository.getPoceteAnkete()

                if (pocete != null) {
                    val ppocete = pocete.filter { it.AnketumId == idAnkete }
                    val max = pocete.maxOf { it.id }
                    val ATID = ppocete.find { it.id.equals(max) }

                    if (ATID != null) {
                        if(!ATID!!.id.equals(AnketaViewModel.targetATID)
                            && AnketaViewModel.targetATID != -1)
                            ATID.id = AnketaViewModel.targetATID

                        if(Connection.isInternetAvailable(MainActivity.context) == false) {
                            return@withContext db.odgovorDAO().getForAT(ATID.id)
                        }

                        val response = ApiAdapter.retrofit.getOdgovoriAnketa(
                            AccountRepository.getHash()!!,
                            ATID.id
                        )
                        if (response != null && response.isSuccessful) {
                            response.body()!!.forEach { db.odgovorDAO().insert(it) }
                            return@withContext response.body()
                        }
                    }
                }

                return@withContext emptyList<Odgovor>()
            }
        }

        suspend fun postaviOdgovorAnketa(idAnketaTaken: Int, idPitanje: Int, odgovor: Int): Int {
            return withContext(Dispatchers.IO) {
                val ovajPokusaj = TakeAnketaRepository.getPoceteAnkete()?.filter {
                    it.id.equals(idAnketaTaken)
                }

                if (ovajPokusaj!!.isNotEmpty()) {
                    val pokusaj = ovajPokusaj.get(0)

                    val pitanja = PitanjeAnketaRepository.getPitanja(pokusaj?.AnketumId!!)

                    val prog = pokusaj.progres + (100.0 / pitanja?.size!!).toInt()

                    val odgovorReq = OdgovorRequest(odgovor, idPitanje, prog)
                    val response = ApiAdapter.retrofit.postaviOdgovorAnketa(
                        AccountRepository.acHash,
                        idAnketaTaken,
                        odgovorReq
                    )
                    val responseBody = response.body()
                    DB.getInstance()!!.odgovorDAO().insert(responseBody!!)

                    if (responseBody.odgovoreno.equals(odgovor)) {
                        var progVal: Int = prog.div(20)

                        if ((progVal + 1) * 20 - prog <= prog - progVal * 20)
                            progVal = (progVal + 1) * 20
                        else
                            progVal *= 20

                        return@withContext progVal
                    } else
                        return@withContext -1
                }

                return@withContext -1
            }
        }
    }
}