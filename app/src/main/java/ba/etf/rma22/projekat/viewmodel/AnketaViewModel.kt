package ba.etf.rma22.projekat.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.AnketaTaken
import ba.etf.rma22.projekat.data.models.Istrazivanje
import ba.etf.rma22.projekat.data.models.Odgovor
import ba.etf.rma22.projekat.data.repositories.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class AnketaViewModel : ViewModel() {
    companion object {
        fun getPoceteAnkete(
            onSuccess: (List<AnketaTaken>) -> Unit,
            onError: () -> Unit
        ) {
            GlobalScope.launch {
                val result = TakeAnketaRepository.getPoceteAnkete()
                when (result) {
                    is List<AnketaTaken> -> onSuccess.invoke(result)
                    else -> onError.invoke()
                }
            }
        }

        fun getAll(
            onSuccess: (List<Anketa>) -> Unit,
            onError: () -> Unit
        ) {
            GlobalScope.launch {
                val result = AnketaRepository.getAll()
                when (result) {
                    is List<Anketa> -> onSuccess.invoke(result)
                    else -> onError.invoke()
                }
            }
        }

        fun getById(
            onSuccess: (Anketa) -> Unit,
            onError: () -> Unit,
            id: Int
        ) {
            GlobalScope.launch {
                val result = AnketaRepository.getById(id)
                when (result) {
                    is Anketa -> onSuccess.invoke(result)
                    else -> onError.invoke()
                }
            }
        }

        fun mapping(
            onSuccess: (List<Istrazivanje>) -> Unit,
            onError: () -> Unit,
            idAnkete: Int
        ) {
            GlobalScope.launch {
                var istrazivanja: List<Istrazivanje> = listOf()
                var out: ArrayList<Istrazivanje> = arrayListOf()
                IstrazivanjaViewModel.getIstrazivanja({ istrazivanja = it }, {})

                while(istrazivanja.isEmpty()) {}

                GrupaRepository.mapping(idAnkete)?.forEach {
                    grupa ->
                        if(istrazivanja.find { it.id.equals(grupa.istrazivanjeID) } != null) {
                            val targetIstrazivanje: Istrazivanje =
                                istrazivanja.find { it.id.equals(grupa.istrazivanjeID) }!!
                            out.add(targetIstrazivanje)
                            grupa.nazivIstraživanja = targetIstrazivanje.naziv
                            if(grupa !in GrupaRepository.grupe) GrupaRepository.grupe.add(grupa)
                        }
                }

                val distinctIstrazivanja = mutableSetOf<Istrazivanje>()
                out.map { distinctIstrazivanja.add(it) }
                out = distinctIstrazivanja.toMutableList() as ArrayList<Istrazivanje>

                onSuccess.invoke(out)
            }
        }

        fun getUpisani(
            onSuccess: (List<Anketa>) -> Unit,
            onError: () -> Unit
        ) {
            GlobalScope.launch {
                val result = AnketaRepository.getUpisane()
                when (result) {
                    is List<Anketa> -> onSuccess.invoke(result)
                    else -> onError.invoke()
                }
            }
        }

        fun upisi(
            onSuccess: (Boolean) -> Unit,
            idGrupe: Int
        ) {
            GlobalScope.launch {
                val result = IstrazivanjeIGrupaRepository.upisiUGrupu(idGrupe)
                when(result) {
                    is Boolean -> onSuccess.invoke(result)
                }
            }
        }

        fun getAnketeIzGrupe(
            onSuccess: (List<Anketa>) -> Unit,
            onError: () -> Unit,
            idGrupe: Int
        ) {
            GlobalScope.launch {
                val result = AnketaRepository.getAnketeIzGrupe(idGrupe)
                Log.d("1 - debug result ankete iz grupe $idGrupe", result.toString())
                when(result) {
                    is List<Anketa> -> onSuccess.invoke(result)
                    else -> onError.invoke()
                }
            }
        }

        fun zapocniOdgovaranje(
            onSuccess: (AnketaTaken) -> Unit,
            onError: () -> Unit,
            idAnkete: Int
        ) {
            GlobalScope.launch {
                val result = TakeAnketaRepository.zapocniAnketu(idAnkete)
                when(result) {
                    is AnketaTaken -> onSuccess.invoke(result)
                    else -> onError.invoke()
                }
            }
        }

        fun posaljiOdgovore(
            onSuccess: () -> Unit,
            onError: () -> Unit,

            idAnketaTaken: Int,
            responses: Map<Int, Int>,
        ) {
            GlobalScope.launch {
                var sinhro: Int = 0

                responses.keys.forEach { idPitanja ->
                    val result = OdgovorRepository.postaviOdgovorAnketa(
                        idAnketaTaken,
                        idPitanja,
                        responses.get(idPitanja)!!
                    )
                    when(result) {
                        is Int -> sinhro++
                    }
                }

                while(sinhro != responses.size) {
                    Log.d("spin", "spinning")
                }

                onSuccess.invoke()
            }
        }

        fun getResponses(
            onSuccess: (List<Odgovor>) -> Unit,
            onError: () -> Unit,

            idAnkete: Int
        ) {
            GlobalScope.launch {
                val result = OdgovorRepository.getOdgovoriAnketa(idAnkete)
                Log.d("debug response body", result.toString())
                when(result) {
                    is List<Odgovor> -> onSuccess.invoke(result)
                    else -> onError.invoke()
                }
            }
        }

        var targetATID: Int = -1
    }
}