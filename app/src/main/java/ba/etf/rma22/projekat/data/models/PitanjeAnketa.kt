package ba.etf.rma22.projekat.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(primaryKeys = ["pitanjeID", "anketaID"])
data class PitanjeAnketa(
    @ColumnInfo(name = "pitanjeID") val pid: Int,
    @ColumnInfo(name = "anketaID") val aid: Int
)