package ba.etf.rma22.projekat.data.models

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Pitanje(
        @PrimaryKey @SerializedName("id") var id: Int,
        @ColumnInfo(name = "naziv") @SerializedName("naziv") var naziv: String,
        @ColumnInfo(name = "tekstPitanja") @SerializedName("tekstPitanja") var tekstPitanja: String,
        @ColumnInfo(name = "opcije") @SerializedName("opcije") var opcije: List<String>,
        @ColumnInfo(name = "anketaFK") var aid: Int
) : Parcelable {
        constructor(parcel: Parcel) : this(
                parcel.readInt(),
                parcel.readString()!!,
                parcel.readString()!!,
                parcel.createStringArrayList() as List<String>,
                parcel.readInt()
        )

        override fun writeToParcel(parcel: Parcel, flags: Int) {
                parcel.writeInt(id)
                parcel.writeString(naziv)
                parcel.writeString(tekstPitanja)
                parcel.writeStringList(opcije)
        }

        override fun describeContents(): Int {
                return 0
        }

        companion object CREATOR : Parcelable.Creator<Pitanje> {
                override fun createFromParcel(parcel: Parcel): Pitanje {
                        return Pitanje(parcel)
                }

                override fun newArray(size: Int): Array<Pitanje?> {
                        return arrayOfNulls(size)
                }
        }
}