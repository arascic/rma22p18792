package ba.etf.rma22.projekat

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.viewpager2.widget.ViewPager2
import ba.etf.rma22.projekat.fragments.FragmentAnkete
import ba.etf.rma22.projekat.fragments.FragmentIstrazivanje
import ba.etf.rma22.projekat.fragments.FragmentPredaj
import ba.etf.rma22.projekat.viewmodel.AccountViewModel
import ba.etf.rma22.projekat.viewmodel.ViewPagerAdapter
import ba.etf.rma22.projekat.data.DB
import ba.etf.rma22.projekat.data.repositories.AccountRepository

class MainActivity : AppCompatActivity() {

    companion object {
        var frags: MutableList<Fragment> = mutableListOf(FragmentAnkete(), FragmentIstrazivanje())

        lateinit var context: Context
    }

    lateinit var viewPager: ViewPager2
    lateinit var callBack: ViewPager2.OnPageChangeCallback

    override fun onResume() {
        super.onResume()

        if(intent.extras != null) {
            val deepLink = intent.extras!!.getString("payload") ?: ""
            if(deepLink.isNotEmpty()) {
                AccountViewModel.deleteAcc({}, {}, AccountRepository.acHash)
                AccountViewModel.dodajAccount({}, {}, deepLink)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        context = this.applicationContext

        if(intent.extras != null) {
            val deepLink = intent.extras!!.getString("payload") ?: ""
            if(deepLink.isNotEmpty()) {
                AccountViewModel.deleteAcc({}, {}, AccountRepository.acHash)
                AccountViewModel.dodajAccount({}, {}, deepLink)
            }
        } else {
            // ovo forsira otvaranje baze podataka pri launch-u
            val sdb: SupportSQLiteDatabase = DB.getInstance(context).openHelper.writableDatabase
        }

        // DEFAULT
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewPager = findViewById(R.id.pager)
        val pagerAdapter = ViewPagerAdapter(supportFragmentManager, frags, this.lifecycle)
        viewPager.adapter = pagerAdapter

        callBack = object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)

                if (pagerAdapter.fragments.get(1).toString().contains("FragmentPoruka")
                    && position == 0) {

                    pagerAdapter.refreshFragment(1, FragmentIstrazivanje())
                }
                else if( position == pagerAdapter.itemCount - 1
                    && !pagerAdapter.fragments.get(0).toString().contains("FragmentAnkete")) {

                    (frags.get(frags.lastIndex) as FragmentPredaj).refreshProgress()
                }
            }
        }

        viewPager.registerOnPageChangeCallback(callBack)
    }

    override fun onDestroy() {
        super.onDestroy()
        viewPager.unregisterOnPageChangeCallback(callBack)
    }

    override fun onBackPressed() {
        if (viewPager.currentItem == 0) {
            super.onBackPressed()
        } else {
            viewPager.currentItem = viewPager.currentItem - 1
        }
    }
}