package ba.etf.rma22.projekat.data.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.Istrazivanje

@Dao
interface IstrazivanjeDAO {
    @Query("delete from Istrazivanje")
    fun drop()

    @Query("select * from Istrazivanje where id <= :offset * 5 and id > (:offset - 1) * 5")
    fun getAll(offset: Int): List<Istrazivanje>

    @Query("select count(*) from Istrazivanje where id <= :offset * 5 and id > (:offset - 1) * 5")
    fun getCount(offset: Int): Int

    @Query("select * from Istrazivanje")
    fun getAll(): List<Istrazivanje>

    @Query("select count(*) from Istrazivanje")
    fun getCount(): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(i: Istrazivanje)

    @Query("select count(*) from Istrazivanje where id = :id")
    fun getByID(id: Int): Int
}