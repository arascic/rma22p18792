package ba.etf.rma22.projekat.data.repositories

import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.DB
import ba.etf.rma22.projekat.data.models.Grupa
import ba.etf.rma22.projekat.data.models.Istrazivanje
import ba.etf.rma22.projekat.data.repositories.IstrazivanjeRepository.Companion.istrazivanja
import ba.etf.rma22.projekat.retrofit.ApiAdapter
import ba.etf.rma22.projekat.retrofit.Connection
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class IstrazivanjeIGrupaRepository {
    companion object {
        val db = DB.getInstance()!!
        val dao = db.istrazivajeDAO()

        suspend fun getIstrazivanja(offset: Int = 0): List<Istrazivanje>? {
            val out: MutableList<Istrazivanje> = mutableListOf()
            lateinit var response: List<Istrazivanje>

            return withContext(Dispatchers.IO) {
                if(Connection.isInternetAvailable(MainActivity.context) == false) {
                    if(offset == 0) {
                        istrazivanja =
                            DB.getInstance()!!.istrazivajeDAO().getAll().toMutableList()
                    }
                    else {
                        istrazivanja =
                            DB.getInstance()!!.istrazivajeDAO().getAll(offset).toMutableList()
                    }

                    return@withContext istrazivanja.toList()
                }

                if(offset != 0) {
                    val res = ApiAdapter.retrofit.getIstrazivanja(offset).body()!!
                    res.forEach {
                        if(dao.getByID(it.id) == 0) dao.insert(it) }
                    return@withContext res
                }
                if(istrazivanja.isNotEmpty())
                    return@withContext istrazivanja

                var i = 1
                while(
                    (ApiAdapter.retrofit.getIstrazivanja(i).body()!!.also { response = it})
                    .isNotEmpty()
                ) {
                    out.addAll(response)
                    i++
                }

                istrazivanja = out

                out.forEach { if(dao.getByID(it.id) == 0) dao.insert(it) }
                return@withContext out
            }
        }

        suspend fun getGrupe(): List<Grupa>? {
            return withContext(Dispatchers.IO) {
                if(db.grupaDAO().hasAll() != 0) {
                    return@withContext db.grupaDAO().getAll()
                }
                else {
                    val response = ApiAdapter.retrofit.getGrupe()
                    val responseBody = response.body()

                    responseBody!!.forEach {
                        if(db.grupaDAO().getByID(it.id) == 0)
                            db.grupaDAO().insert(it)
                    }

                    return@withContext responseBody
                }
            }
        }

        suspend fun getGrupeZaIstrazivanje(idIstrazivanja: Int): List<Grupa>? {
            return withContext(Dispatchers.IO) {
                lateinit var responseBody: List<Grupa>

                if(Connection.isInternetAvailable(MainActivity.context) == false) {
                    responseBody = DB.getInstance()!!.grupaDAO().getAll()
                }
                else
                    responseBody = ApiAdapter.retrofit.getGrupe().body()!!

                val out: MutableList<Grupa> = mutableListOf()

                for(x: Grupa in responseBody!!) {
                    if(x.istrazivanjeID == idIstrazivanja)
                        out.add(x)
                }

                return@withContext out
            }
        }

        suspend fun upisiUGrupu(idGrupa: Int): Boolean {
            return withContext(Dispatchers.IO) {
                val response = ApiAdapter.retrofit.upisiUGrupu(idGrupa, AccountRepository.acHash)
                if(response.body()?.value?.contains("je dodan u grupu", ignoreCase = true)!!) {
                    db.grupaDAO().upišiGrupu(idGrupa)
                    return@withContext true
                }
                return@withContext false
            }
        }

        suspend fun getUpisaneGrupe(): List<Grupa>? {
            return withContext(Dispatchers.IO) {
                if(Connection.isInternetAvailable(MainActivity.context) == false) {
                    return@withContext DB.getInstance()!!.grupaDAO().getUpisane()
                }

                val response = ApiAdapter.retrofit.getUpisaneGrupe(AccountRepository.acHash)
                val responseBody = response.body()

                ApiAdapter.retrofit.getGrupe().body()!!.forEach {
                    if(it in responseBody!!)
                        it.upisana = 1
                    if(db.grupaDAO().getByID(it.id) == 0) db.grupaDAO().insert(it)
                }

                return@withContext responseBody
            }
        }
    }
}