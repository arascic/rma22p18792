package ba.etf.rma22.projekat.data

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*

object Converter {
    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }

    @TypeConverter
    fun stringToMap(value: String): Map<Int, Int> {
        if(value.equals("")) return mapOf()

        // {1=1, 2=2, 3=3}
        val out = mutableMapOf<Int, Int>()

        value.removeSurrounding("{", "}").split(",").forEach {
            it.trimStart()
            val item = it.split("=").map { Integer.parseInt(it) }
            out.put(item.get(0), item.get(1))
        }

        return out
    }

    @TypeConverter
    fun mapToString(map: Map<Int, Int>?): String {
        if(map != null) return map.toString()
        else return ""
    }

    @TypeConverter
    fun listToString(ls: List<String>): String {
        val gson = Gson()
        val type = object : TypeToken<List<String>>() {}.type
        return gson.toJson(ls, type)
    }

    @TypeConverter
    fun stringToList(value: String): List<String> {
        val gson = Gson()
        val type = object : TypeToken<List<String>>() {}.type
        return gson.fromJson(value, type)
    }
}