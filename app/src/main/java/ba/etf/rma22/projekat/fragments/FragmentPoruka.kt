package ba.etf.rma22.projekat.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.R

class FragmentPoruka(private var grupa: String, private var istrazivanje: String) : Fragment() {

    lateinit private var anketa: Anketa

    private lateinit var msg: TextView
    private lateinit var topView: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        topView = inflater.inflate(R.layout.fragment_poruka, container, false)

        msg = topView.findViewById(R.id.tvPoruka)
        if(grupa != "" && istrazivanje != "") {
            msg.setText("Uspješno ste upisani u grupu ${grupa} istraživanja ${istrazivanje}!")
        } else {
            msg.setText("Završili ste anketu ${anketa.naziv} u okviru istraživanja ${anketa.nazivIstrazivanja}")
        }

        return topView
    }

    companion object {
        fun newInstance(grupa: String, istrazivanje: String) =
                FragmentPoruka(grupa, istrazivanje)

        fun newInstance(anketa: Anketa): FragmentPoruka {
            val frag = FragmentPoruka("", "")

            frag.anketa = anketa

            return frag
        }
    }
}