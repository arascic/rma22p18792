package ba.etf.rma22.projekat.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.daos.*
import ba.etf.rma22.projekat.data.models.*

@Database(entities = arrayOf(
    Account::class, Grupa::class, Anketa::class, Pitanje::class, Istrazivanje::class, Odgovor::class,
    AnketaTaken::class, AnketaGrupa::class, PitanjeAnketa::class
), version = 1)
@TypeConverters(Converter::class)
abstract class DB : RoomDatabase() {
    abstract fun accountDAO(): AccountDAO
    abstract fun grupaDAO(): GrupaDAO
    abstract fun anketaDAO(): AnketaDAO
    abstract fun pitanjeDAO(): PitanjeDAO
    abstract fun istrazivajeDAO(): IstrazivanjeDAO
    abstract fun odgovorDAO(): OdgovorDAO
    abstract fun anketaTakenDAO(): AnketaTakenDAO
    abstract fun anketaGrupaDAO(): AnketaGrupaDAO
    abstract fun pitanjeAnketaDAO(): PitanjeAnketaDAO

    fun dropUser() {
        singleton!!.accountDAO().drop()
        singleton!!.grupaDAO().drop()
        singleton!!.anketaDAO().drop()
        singleton!!.pitanjeDAO().drop()
        singleton!!.istrazivajeDAO().drop()
        singleton!!.odgovorDAO().drop()
        singleton!!.anketaTakenDAO().drop()
        singleton!!.anketaGrupaDAO().drop()
        singleton!!.pitanjeAnketaDAO().drop()
    }

    companion object {
        private var singleton: DB? = null

        fun setInstance(appdb: DB): Unit {
            singleton = appdb
        }

        fun getInstance(context: Context): DB {
            if (singleton == null) {
                synchronized(DB::class) {
                    singleton = buildRoomDB(context)
                }
            }
            return singleton!!
        }

        fun getInstance(): DB? {
            if(singleton != null) return singleton!!
            else return getInstance(MainActivity.context)
        }

        private fun buildRoomDB(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                DB::class.java,
                "RMA22DB"
            ).build()
    }
}