package ba.etf.rma22.projekat.data.daos

import androidx.room.*
import ba.etf.rma22.projekat.data.models.Grupa

@Dao
interface GrupaDAO {
    @Query("delete from Grupa")
    fun drop()

    @Query("select count(*) from Grupa where upisana = 1")
    fun hasUpisane(): Int

    @Query("select * from Grupa where upisana = 1")
    fun getUpisane(): List<Grupa>

    @Query("select count(*) from Grupa")
    fun hasAll(): Int

    @Query("select * from Grupa")
    fun getAll(): List<Grupa>

    @Query("update Grupa set upisana = 1 where id = :gid")
    fun upišiGrupu(gid: Int)

    @Query("select count(*) from Grupa where id = :gid")
    fun getByID(gid: Int): Int

    @Query("select * from Grupa where id = :gid")
    fun getObjByID(gid: Int): Grupa

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(g: Grupa)
}