package ba.etf.rma22.projekat.data.models

import android.os.Parcel
import android.os.Parcelable
import androidx.room.*
import ba.etf.rma22.projekat.data.repositories.GrupaRepository
import com.google.gson.annotations.SerializedName

@Entity
data class Grupa(
    @PrimaryKey @SerializedName("id") val id: Int,
    @ColumnInfo(name = "naziv") @SerializedName("naziv") val naziv: String,
    @ColumnInfo(name = "kreirana") @SerializedName("createdAt") val kreirana: String,
    @ColumnInfo(name = "promijenjena") @SerializedName("updatedAt") val promijenjena: String,
    @ColumnInfo(name = "istrazivanjeID") @SerializedName("IstrazivanjeId") val istrazivanjeID: Int
) : Parcelable {

    @Ignore
    var nazivIstraživanja: String = ""

    @ColumnInfo(name = "upisana") var upisana: Int = 0

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readInt()
    )

    override fun toString(): String {
        return naziv
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(naziv)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Grupa> {
        override fun createFromParcel(parcel: Parcel): Grupa {
            return Grupa(parcel)
        }

        override fun newArray(size: Int): Array<Grupa?> {
            return arrayOfNulls(size)
        }
    }
}