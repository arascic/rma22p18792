package ba.etf.rma22.projekat.data.daos

import androidx.room.*
import ba.etf.rma22.projekat.data.models.Odgovor

@Dao
interface OdgovorDAO {
    @Query("delete from Odgovor")
    fun drop()

    @Query("select count(*) from Odgovor where ATID=:aid")
    fun hasCacheFor(aid: Int): Int

    @Query("select * from Odgovor where ATID=:ATID")
    fun getForAT(ATID: Int): List<Odgovor>

    @Query("select count(*) from Odgovor where PitanjeID = :pid")
    fun hasOdg(pid: Int): Int

    @Update
    fun putOdg(odg: Odgovor)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(odg: Odgovor)
}