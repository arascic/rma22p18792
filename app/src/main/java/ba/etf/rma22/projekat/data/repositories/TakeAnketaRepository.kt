package ba.etf.rma22.projekat.data.repositories

import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.DB
import ba.etf.rma22.projekat.data.models.AnketaTaken
import ba.etf.rma22.projekat.retrofit.ApiAdapter
import ba.etf.rma22.projekat.retrofit.Connection
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*

class TakeAnketaRepository {
    companion object {
        suspend fun zapocniAnketu(idAnketa: Int): AnketaTaken? {
            return withContext(Dispatchers.IO) {
                AnketaRepository.getById(idAnketa)?.datumRada = Calendar.getInstance().time
                val response = ApiAdapter.retrofit.zapocniAnketu(AccountRepository.acHash, idAnketa)
                val responseBody = response.body()
                responseBody?.AnketaId = idAnketa

                if(responseBody!!.id != null)
                    DB.getInstance()!!.anketaTakenDAO().insert(responseBody!!)
                return@withContext responseBody
            }
        }

        suspend fun getPoceteAnkete(): List<AnketaTaken>? {
            return withContext(Dispatchers.IO) {
                if(Connection.isInternetAvailable(MainActivity.context) == false)
                    return@withContext DB.getInstance()!!.anketaTakenDAO().getAll()

                val db = DB.getInstance()!!
                val response = ApiAdapter.retrofit.getPoceteAnkete(AccountRepository.acHash)
                val responseBody = response.body()
                if (responseBody?.size == 0)
                    return@withContext null
                responseBody!!.forEach {
                    if(db.anketaTakenDAO().hasID(it.id) == 0)
                        db.anketaTakenDAO().insert(it)
                }
                return@withContext responseBody
            }
        }
    }
}