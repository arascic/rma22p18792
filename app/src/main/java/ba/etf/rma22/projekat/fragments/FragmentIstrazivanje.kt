package ba.etf.rma22.projekat.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.view.isEmpty
import androidx.core.view.isNotEmpty
import androidx.core.view.size
import androidx.fragment.app.Fragment
import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.models.Grupa
import ba.etf.rma22.projekat.data.models.Istrazivanje
import ba.etf.rma22.projekat.data.models.Korisnik
import ba.etf.rma22.projekat.data.repositories.GrupaRepository
import ba.etf.rma22.projekat.data.repositories.IstrazivanjeIGrupaRepository
import ba.etf.rma22.projekat.data.repositories.IstrazivanjeRepository
import ba.etf.rma22.projekat.viewmodel.AnketaViewModel
import ba.etf.rma22.projekat.viewmodel.IstrazivanjaViewModel
import ba.etf.rma22.projekat.viewmodel.ViewPagerAdapter
import ba.etf.rma22.projekat.R
import ba.etf.rma22.projekat.retrofit.Connection
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.runBlocking
import java.lang.RuntimeException
import java.util.stream.IntStream.range

class FragmentIstrazivanje :
        Fragment(),
        AdapterView.OnItemSelectedListener,
        View.OnClickListener {

    private lateinit var topView: View
    private lateinit var odabirGodina: Spinner // default posljednja izabrana
    private lateinit var odabirIstrazivanja: Spinner // samo gdje nije upisan
    private lateinit var odabirGrupa: Spinner
    private lateinit var adapterIstrazivanja: ArrayAdapter<String>
    private lateinit var adapterGrupa: ArrayAdapter<String>
    private lateinit var svaIstrazivanja: List<Istrazivanje>
    private var selekcijaIstrazivanja: MutableList<String> = mutableListOf("")
    private var selekcijaGrupa: MutableList<String> = mutableListOf("")
    private lateinit var dugme: Button

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        topView = inflater.inflate(R.layout.fragment_istrazivanje, container, false)

        IstrazivanjaViewModel.getIstrazivanja({ svaIstrazivanja = it }, {})

        // SPINNER GODINA
        odabirGodina = topView.findViewById(R.id.odabirGodina)
        ArrayAdapter.createFromResource(
                requireContext(),
                R.array.godine_array,
                android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(
                    android.R.layout.simple_spinner_dropdown_item)
            odabirGodina.adapter = adapter
        }

        if(FragmentAnkete.posljednjiOdabir.equals(""))
            odabirGodina.setSelection(0)
        else
            odabirGodina.setSelection(getIndex(odabirGodina, FragmentAnkete.posljednjiOdabir))

        odabirGodina.onItemSelectedListener = this

        /**************************************************************/

        // SPINNER ISTRAŽIVANJA
        odabirIstrazivanja = topView.findViewById(R.id.odabirIstrazivanja)

        runBlocking {
            val upisani = IstrazivanjeRepository.getUpisani().map { it.naziv }

            Log.d("debug", IstrazivanjeRepository.getUpisani().toString())

            selekcijaIstrazivanja = IstrazivanjeRepository.getIstrazivanjeByGodina(
                Integer.parseInt(odabirGodina.selectedItem.toString())
            ).map { it.naziv }.filter { !(it in upisani) }.toMutableList()
        }

        adapterIstrazivanja = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_item,
            selekcijaIstrazivanja
        )
        adapterIstrazivanja.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        odabirIstrazivanja.adapter = adapterIstrazivanja
        odabirIstrazivanja.onItemSelectedListener = this

        /**************************************************************/

        // SPINNER GRUPA
        odabirGrupa = topView.findViewById(R.id.odabirGrupa)

        if(odabirIstrazivanja.isNotEmpty())
            selekcijaGrupa = GrupaRepository.getGroupsByIstrazivanje(
                odabirIstrazivanja.selectedItem.toString()
            ).map { it.naziv }.toMutableList()
        else
            selekcijaGrupa = mutableListOf()

        adapterGrupa = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_item,
            selekcijaGrupa
        )
        adapterGrupa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        odabirGrupa.adapter = adapterGrupa
        odabirGrupa.onItemSelectedListener = this

        /**************************************************************/

        // DUGME
        dugme = topView.findViewById(R.id.dodajIstrazivanjeDugme)

        if(selekcijaGrupa.isNotEmpty()) dugme.isEnabled = true
        else dugme.isEnabled = false

        dugme.transformationMethod = null
        dugme.setOnClickListener(this)

        /***/

        if(!Connection.isInternetAvailable(MainActivity.context)) {
            Snackbar.make(
                (requireActivity() as MainActivity).viewPager,
                "NO CONNECTION",
                Snackbar.LENGTH_SHORT
            ).show()
            odabirGodina.isEnabled = false
            odabirGrupa.isEnabled = false
            odabirIstrazivanja.isEnabled = false
            dugme.isEnabled = false
        }

        return topView
    }

    fun getIndex(spinner: Spinner, myString: String): Int {
        for(i in range(0, spinner.count)) {
            if(spinner.getItemAtPosition(i).toString().equals(myString))
                return i;
        }
        return -1
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        if(p0 == odabirGodina) {
            FragmentAnkete.posljednjiOdabir = odabirGodina.selectedItem.toString()
            selekcijaIstrazivanja = svaIstrazivanja.filter {
                it.godina.toString() == odabirGodina.selectedItem }.map {
                it.naziv }.toMutableList()

            runBlocking {
                selekcijaIstrazivanja = selekcijaIstrazivanja.filter {
                    !(it in IstrazivanjeRepository.getUpisani().map { it.naziv })
                }.toMutableList()
            }

            adapterIstrazivanja.clear()
            adapterIstrazivanja.addAll(selekcijaIstrazivanja)
            adapterIstrazivanja.notifyDataSetChanged()
            odabirIstrazivanja.adapter = adapterIstrazivanja

            if(odabirIstrazivanja.selectedItem != null) {
                selekcijaGrupa = GrupaRepository.getGroupsByIstrazivanje(
                        odabirIstrazivanja.selectedItem.toString()).map { it.naziv }.toMutableList()

                adapterGrupa.clear()
                adapterGrupa.addAll(selekcijaGrupa)
                adapterGrupa.notifyDataSetChanged()
            }

            if(selekcijaIstrazivanja.isEmpty()) {
                adapterGrupa.clear()
                selekcijaGrupa.clear()
                adapterGrupa.notifyDataSetChanged()
                dugme.isEnabled = false
            }
        }
        else if(p0 == odabirIstrazivanja) {
            selekcijaGrupa = GrupaRepository.getGroupsByIstrazivanje(
                    odabirIstrazivanja.selectedItem.toString()).map { it.naziv }.toMutableList()

            adapterGrupa.clear()
            adapterGrupa.addAll(selekcijaGrupa)
            adapterGrupa.notifyDataSetChanged()

            if(selekcijaGrupa.isNotEmpty()) dugme.isEnabled = true
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        return
    }

    override fun onClick(p0: View?) {

        val grupa: Grupa = GrupaRepository.getGroupByName(odabirGrupa.selectedItem.toString())

        Korisnik.upisan.add(grupa)
        AnketaViewModel.upisi(
            {
                if (!it) throw RuntimeException("NIJE MOGUCE UPISATI GRUPU")
                else {
                    val frag = FragmentPoruka.newInstance(
                        odabirGrupa.selectedItem.toString(),
                        odabirIstrazivanja.selectedItem.toString()
                    )

                    (requireActivity() as MainActivity).runOnUiThread {
                        adapterGrupa.remove(odabirGrupa.selectedItem.toString())
                        adapterGrupa.notifyDataSetChanged()
                        (MainActivity.frags.get(0) as FragmentAnkete).refresh()

                        val pagerAdapter =
                            ((requireActivity() as MainActivity).viewPager.adapter as ViewPagerAdapter)

                        pagerAdapter.refreshFragment(1, frag)
                    }
                }
            },
            grupa.id
        )
    }
}