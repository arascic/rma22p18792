package ba.etf.rma22.projekat.data.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma22.projekat.data.models.Pitanje

@Dao
interface PitanjeDAO {
    @Query("delete from Pitanje")
    fun drop()

    @Query("select count(*) from Pitanje where anketaFK = :aid")
    fun hasCachedAnswers(aid: Int): Int

    @Query("select * from Pitanje where anketaFK = :aid")
    fun getPitanjaForAnketa(aid: Int): List<Pitanje>

    @Query("select * from Pitanje where id = :pid")
    fun getPitanje(pid: Int): Pitanje

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun keširajPitanja(pitanje: Pitanje)
}