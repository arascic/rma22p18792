package ba.etf.rma22.projekat.data.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import ba.etf.rma22.projekat.data.models.AnketaGrupa
import ba.etf.rma22.projekat.data.models.Grupa

@Dao
interface AnketaGrupaDAO {
    @Query("select distinct g.id from Grupa as g, AnketaGrupa as ag " +
            "where g.id = ag.grupaID and ag.anketaID = :aid")
    fun getGrupeAnkete(aid: Int): List<Int>

    @Query("select distinct anketaID from AnketaGrupa where grupaID = :gid")
    fun getAnketeGrupe(gid: Int): List<Int>

    @Insert(onConflict = REPLACE)
    fun insert(ag: AnketaGrupa)

    @Query("delete from AnketaGrupa")
    fun drop()

    @Query("select count(*) from AnketaGrupa where anketaID = :aid")
    fun hasAnketa(aid: Int): Int
}