package ba.etf.rma22.projekat.data.repositories

import android.util.Log
import ba.etf.rma22.projekat.data.models.Istrazivanje
import ba.etf.rma22.projekat.data.models.Korisnik
import kotlinx.coroutines.runBlocking

class IstrazivanjeRepository {
    companion object {
        var istrazivanja: MutableList<Istrazivanje> = mutableListOf()

        fun getUpisani(): List<Istrazivanje> {
            val upisani = Korisnik.getUpisani()

            Log.d("debug upisani", upisani.map{ it.nazivIstraživanja }.toString())

            return istrazivanja.filter {
                it.naziv in upisani.map { it.nazivIstraživanja }
            }
        }

        fun getAll(): List<Istrazivanje> {
            return istrazivanja
        }

        fun getIstrazivanjeByGodina(godina: Int) : List<Istrazivanje> {
            return istrazivanja.filter { it.godina.equals(godina) }
        }
    }
}