package ba.etf.rma22.projekat.data.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma22.projekat.data.models.PitanjeAnketa

@Dao
interface PitanjeAnketaDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(ag: PitanjeAnketa)

    @Query("select pitanjeID from PitanjeAnketa where anketaID = :aid")
    fun getPitanja(aid: Int): List<Int>

    @Query("delete from PitanjeAnketa")
    fun drop()
}