package ba.etf.rma22.projekat.data.repositories

import android.util.Log
import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.DB
import ba.etf.rma22.projekat.data.models.Pitanje
import ba.etf.rma22.projekat.data.models.PitanjeAnketa
import ba.etf.rma22.projekat.retrofit.ApiAdapter
import ba.etf.rma22.projekat.retrofit.Connection
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PitanjeAnketaRepository {
    companion object {
        suspend fun getPitanja(idAnkete: Int): List<Pitanje>? {
            return withContext(Dispatchers.IO) {
                val db = DB.getInstance()!!

                if(Connection.isInternetAvailable(MainActivity.context) == false) {
                    val out = mutableListOf<Pitanje>()

                    db.pitanjeAnketaDAO().getPitanja(idAnkete).forEach {
                        out.add(db.pitanjeDAO().getPitanje(it))
                    }

                    return@withContext out
                }

                val response = ApiAdapter.retrofit.getPitanja(idAnkete)
                val responseBody = response.body()
                responseBody!!.forEach {
                    it.aid = idAnkete
                    db.pitanjeDAO().keširajPitanja(it)
                    Log.d("DEBUG", it.toString())
                    val pa = PitanjeAnketa(it.id, idAnkete)
                    db.pitanjeAnketaDAO().insert(pa)
                }
                return@withContext responseBody
            }
        }
    }
}