package ba.etf.rma22.projekat.viewmodel

import ba.etf.rma22.projekat.data.repositories.AccountRepository
import kotlinx.coroutines.*

class AccountViewModel {
    companion object {
        val scope = CoroutineScope(Job() + Dispatchers.Main)

        fun postaviHash(
            onSuccess: (Boolean) -> Unit,
            onError: () -> Unit,
            hash: String
        ) {
            scope.launch {
                val result = AccountRepository.postaviHash(hash)
                when (result) {
                    is Boolean -> onSuccess.invoke(result)
                    else -> onError.invoke()
                }
            }
        }

        fun dodajAccount(
            onSuccess: (Boolean) -> Unit,
            onError: () -> Unit,
            hash: String
        ) {
            scope.launch {
                val result = AccountRepository.dodajAccount(hash)
                when(result) {
                    is Boolean -> onSuccess.invoke(result)
                    else -> onError.invoke()
                }
            }
        }

        fun deleteAcc(
            onSuccess: (Boolean) -> Unit,
            onError: () -> Unit,
            hash: String
        ) {
            scope.launch {
                val result = AccountRepository.deleteAccData(hash)
                when(result) {
                    is Boolean -> onSuccess.invoke(result)
                    else -> onError.invoke()
                }
            }
        }
    }
}