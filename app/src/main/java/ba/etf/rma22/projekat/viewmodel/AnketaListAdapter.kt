package ba.etf.rma22.projekat.viewmodel

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.repositories.AnketaRepository
import ba.etf.rma22.projekat.fragments.FragmentAnkete.Companion.dataLoad
import ba.etf.rma22.projekat.R
import java.text.SimpleDateFormat

class AnketaListAdapter(
    private var anketaRepo: MutableList<Anketa>,
    val listener: CustomListener
) : RecyclerView.Adapter<AnketaListAdapter.AnketaViewHolder>() {

    inner class AnketaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nazivAnkete = itemView.findViewById<TextView>(R.id.naziv)
        val statusAnkete = itemView.findViewById<ImageView>(R.id.status)
        val imeIstraživanja = itemView.findViewById<TextView>(R.id.nazivIstraživanja)
        val progres = itemView.findViewById<ProgressBar>(R.id.progresZavrsetka)
        val datum = itemView.findViewById<TextView>(R.id.datum)
        val poruka = itemView.findViewById<TextView>(R.id.poruka)

        fun bind(anketa: Anketa, listener: CustomListener) {
            itemView.setOnClickListener {
                listener.onItemClicked(anketa)
            }
        }
    }

    interface CustomListener {
        fun onItemClicked(anketa: Anketa)
    }

    inner class Callback(private val oldList: List<Anketa>, private val newList: List<Anketa>) : DiffUtil.Callback() {
        override fun getOldListSize(): Int {
            return oldList.size
        }

        override fun getNewListSize(): Int {
            return newList.size
        }

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList.get(oldItemPosition).equals(newList.get(newItemPosition))
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList.get(oldItemPosition).naziv.equals(newList.get(newItemPosition).naziv)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnketaViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_anketa, parent, false)
        return AnketaViewHolder(view)
    }

    override fun getItemCount(): Int {
        return anketaRepo.size
    }

    // private var done: List<Anketa> = AnketaRepository.getDone()
    private var activeNotDone: List<Anketa> = AnketaRepository.getActiveNotDone()
    private var nextToBeActive: List<Anketa> = AnketaRepository.getNextToBeActive()
    private var pastNotDone: List<Anketa> = AnketaRepository.getPastNotDone()

    override fun onBindViewHolder(holder: AnketaViewHolder, position: Int) {
        holder.imeIstraživanja.text = anketaRepo[position].nazivIstrazivanja
        holder.nazivAnkete.text = anketaRepo[position].naziv

        holder.bind(anketaRepo[position], listener)

        val anketa: Anketa = anketaRepo[position]
        val format = SimpleDateFormat("dd.MM.yyyy")
        if(anketa in AnketaRepository.getDone()) {
            // plava
            holder.statusAnkete.setImageResource(R.drawable.plava)
            holder.poruka.text = "Anketa urađena: "
            holder.datum.text = format.format(anketa.datumRada)
        } else if(anketa in activeNotDone) {
            // zelena
            holder.statusAnkete.setImageResource(R.drawable.zelena)
            holder.poruka.text = "Vrijeme zatvaranja: "

            if(anketa.datumKrajaD != null)
                holder.datum.text = format.format(anketa.datumKrajaD)
            else
                holder.datum.text = "Do daljnjeg"
        } else if(anketa in nextToBeActive) {
            // žuta
            holder.statusAnkete.setImageResource(R.drawable.zuta)
            holder.poruka.text = "Vrijeme aktiviranja: "
            holder.datum.text = format.format(anketa.datumPocetkaD)
        } else if(anketa in pastNotDone) {
            // crvena
            holder.statusAnkete.setImageResource(R.drawable.crvena)
            holder.poruka.text = "Anketa zatvorena: "
            holder.datum.text = format.format(anketa.datumKrajaD)
        }

        // progres
        val realProg = anketaRepo[position].progres
        var progVal : Int = anketaRepo[position].progres.toInt().div(20)

        if((progVal + 1) * 20 - realProg < realProg - progVal * 20)
            progVal = (progVal + 1) * 20
        else
            progVal = progVal * 20

        holder.progres.setProgress(progVal)
    }

    fun updateAnketa(update: List<Anketa>) {
        update.forEach { it1 ->
            it1.fixUp();
            val istrazivanja: String = dataLoad.getCompleted()
                .find { it.naziv.equals(it1.naziv) }!!.nazivIstrazivanja!!
            it1.nazivIstrazivanja = istrazivanja
        }

        anketaRepo.clear()
        anketaRepo.addAll(update.toSet().toList())
        anketaRepo = anketaRepo.sortedWith { lhs, rhs ->
            if (lhs.datumPocetkaD < rhs.datumPocetkaD) -1 else 1
        }.toMutableList()
    }
}