package ba.etf.rma22.projekat.data.models

import android.annotation.SuppressLint
import android.os.Parcel
import android.os.Parcelable
import androidx.room.*
import ba.etf.rma22.projekat.viewmodel.AnketaViewModel
import com.google.gson.annotations.SerializedName
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("SimpleDateFormat")
@Entity
data class Anketa (
    @PrimaryKey @SerializedName("id") val id: Int,
    @ColumnInfo(name = "naziv") @SerializedName("naziv") val naziv: String,
    @ColumnInfo(name = "datumPocetak") @SerializedName("datumPocetak") val datumPocetak: String,
    @ColumnInfo(name = "datumKraj") @SerializedName("datumKraj") val datumKraj: String?,
    @ColumnInfo(name = "trajanje") @SerializedName("trajanje") val trajanje: Int
) : Parcelable {

    @ColumnInfo(name = "grupaFK") var gid: Int = -1

    @Ignore var nazivIstrazivanja: String = ""
    @Ignore var datumRada: Date? = null
    @Ignore var nazivGrupe: String = ""
    @Ignore var progres: Float = 0F

    @Ignore lateinit var datumPocetkaD: Date
    @Ignore var datumKrajaD: Date? = null

    @Ignore var responses: MutableMap<Int, Int>? = mutableMapOf()
    @Ignore var anketaTaken: Int = -1

    @Ignore var busy: Boolean = false

    fun computeAnketaTaken() {
        busy = true
        AnketaViewModel.getPoceteAnkete(
            {
                val query = it.filter { it.AnketumId == id }

                if(query.isNotEmpty()) {
                    anketaTaken = query.maxOf { it.id }
                    progres = query.filter { it.id == anketaTaken }.get(0).progres.toFloat()
                }

                busy = false
            },
            { busy = false }
        )
    }

    fun computeResponses() {
        busy = true
        responses = mutableMapOf()
        if(anketaTaken != -1) {
            AnketaViewModel.getResponses(
                { odgovori ->
                    odgovori.forEach {
                        responses!!.put(it.id, it.odgovoreno)
                    }
                    busy = false
                },
                { busy = false },
                id
            )
        }
        else busy = false
    }

    fun fixUp() {
        val parser = SimpleDateFormat("yyyy-mm-dd")

        datumPocetkaD = parser.parse(datumPocetak)!!

        if(datumKraj != null)
            datumKrajaD = parser.parse(datumKraj)!!
    }

    @SuppressLint("SimpleDateFormat")
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString(),
        parcel.readInt()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(naziv)
        parcel.writeString(datumPocetak)
        parcel.writeString(datumKraj)
        parcel.writeInt(trajanje)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun toString(): String {
        return "Anketa(id=$id, naziv='$naziv', datumPocetkaString='$datumPocetak'," +
                " datumKrajString=$datumKraj, trajanje=$trajanje," +
                " nazivIstrazivanja='$nazivIstrazivanja')"
    }

    companion object CREATOR : Parcelable.Creator<Anketa> {
        override fun createFromParcel(parcel: Parcel): Anketa {
            return Anketa(parcel)
        }

        override fun newArray(size: Int): Array<Anketa?> {
            return arrayOfNulls(size)
        }
    }
}