package ba.etf.rma22.projekat.data.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ba.etf.rma22.projekat.data.models.AnketaTaken

@Dao
interface AnketaTakenDAO {
    @Query("delete from AnketaTaken")
    fun drop()

    @Query("select * from AnketaTaken")
    fun getAll(): List<AnketaTaken>

    @Query("select count(*) from AnketaTaken")
    fun hasCached(): Int

    @Query("select count(*) from AnketaTaken where id = :id")
    fun hasID(id: Int): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(at: AnketaTaken)
}