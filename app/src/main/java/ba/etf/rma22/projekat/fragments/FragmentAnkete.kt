package ba.etf.rma22.projekat.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.MainActivity.Companion.frags
import ba.etf.rma22.projekat.R
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.AnketaState
import ba.etf.rma22.projekat.data.models.Pitanje
import ba.etf.rma22.projekat.data.repositories.AnketaRepository
import ba.etf.rma22.projekat.data.repositories.AnketaRepository.Companion.pripremiAnkete
import ba.etf.rma22.projekat.data.repositories.PitanjeAnketaRepository
import ba.etf.rma22.projekat.retrofit.Connection
import ba.etf.rma22.projekat.viewmodel.AnketaListAdapter
import ba.etf.rma22.projekat.viewmodel.AnketaViewModel
import ba.etf.rma22.projekat.viewmodel.ViewPagerAdapter
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.*
import java.util.stream.IntStream.range


class FragmentAnkete : Fragment(), AdapterView.OnItemSelectedListener, AnketaListAdapter.CustomListener {

    private lateinit var topView: View
    private lateinit var ankete: List<Anketa>
    private lateinit var view: RecyclerView
    private lateinit var spinner: Spinner

    companion object {
        var posljednjiOdabir: String = ""
        lateinit var adapter: AnketaListAdapter

        lateinit var dataLoad: Deferred<List<Anketa>>

        var urađeneAnkete: MutableList<Anketa> = mutableListOf()
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View{

        topView = inflater.inflate(R.layout.fragment_ankete, container, false)

        view = topView.findViewById(R.id.listaAnketa)
        AnketaRepository.sortiraj()
        val insider = this

        runBlocking {
            dataLoad = GlobalScope.async(Dispatchers.Default) { pripremiAnkete() }
            dataLoad.await()
        }

        // adapter = AnketaListAdapter(AnketaRepository.getAll().toMutableList(), insider)
        adapter = AnketaListAdapter(dataLoad.getCompleted().toMutableList(), insider)
        ankete = dataLoad.getCompleted()

        val names = urađeneAnkete.map { it.naziv }
        ankete.forEach { it1 ->
            if(it1 != null) {
                if (it1.naziv in names) {
                    val target = urađeneAnkete.filter { it.naziv.equals(it1.naziv) }[0]

                    it1.datumRada = target.datumRada
                }
            }
        }

        view.adapter = adapter
        view.layoutManager = NpaGridLayoutManager(
            requireActivity(), 2,
            RecyclerView.VERTICAL, false
        )

        // SPINNER
        spinner = topView.findViewById(R.id.filterAnketa)
        ArrayAdapter.createFromResource(
                requireActivity(),
                R.array.spinner_array,
                android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
        }
        spinner.onItemSelectedListener = this
        spinner.setSelection(1) // sve ankete

        return topView
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        val izbor = p0?.getItemAtPosition(p2).toString()
        recyclerRefresh(izbor)
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun recyclerRefresh(izbor: String) {
        if(izbor.equals("Sve ankete")) {
            adapter.updateAnketa(ankete)
            adapter.notifyDataSetChanged()
        } else if(izbor.equals("Sve moje ankete")) {
            AnketaViewModel.getUpisani({
                it.forEach {
                    it.computeAnketaTaken()
                    while(it.busy!!) {
                        Log.d("spin", "spin")
                    }
                }
                adapter.updateAnketa(it)
                // Log.d("debug u fragmentu", it.toString())
                (requireActivity() as MainActivity).runOnUiThread { adapter.notifyDataSetChanged() }
            }, {})
        } else if(izbor.equals("Urađene ankete")) {
            adapter.updateAnketa(AnketaRepository.getDone())
            adapter.notifyDataSetChanged()
        } else if(izbor.equals("Buduće ankete")) {
            adapter.updateAnketa(AnketaRepository.getFuture())
            adapter.notifyDataSetChanged()
        } else if(izbor.equals("Prošle ankete")) {
            adapter.updateAnketa(AnketaRepository.getAllPast())
            adapter.notifyDataSetChanged()
        }
    }

    fun refresh() {
        recyclerRefresh(topView.findViewById<Spinner>(R.id.filterAnketa).selectedItem.toString())
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onNothingSelected(p0: AdapterView<*>?) {
        adapter.updateAnketa(ankete)
        adapter.notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onItemClicked(anketa: Anketa) {
        runBlocking {
            if(AnketaState.getState(anketa) == AnketaState.NOT_ACTIVE ||
                !AnketaRepository.getMyAnkete().contains(anketa))
                    return@runBlocking

            val adapter: ViewPagerAdapter = (requireActivity() as MainActivity).viewPager.adapter
                as ViewPagerAdapter

            anketa.computeAnketaTaken()

            while(anketa.busy) { Log.d("spin1", "spin") }

            AnketaViewModel.targetATID = anketa.anketaTaken

            anketa.computeResponses()

            while(anketa.busy) { Log.d("spin2", "spin") }

            var pitanja: List<Pitanje>
            pitanja = PitanjeAnketaRepository.getPitanja(anketa.id)!!

            val newFrags = mutableListOf<Fragment>()
            for (i in range(0, pitanja.size))
                newFrags.add(i, FragmentPitanje(pitanja.get(i), anketa))
            newFrags.add(newFrags.size, FragmentPredaj(anketa))

            if(Connection.isInternetAvailable(MainActivity.context) == true) {
                AnketaViewModel.zapocniOdgovaranje({
                    FragmentPitanje.anketaTaken = it
                }, {}, anketa.id)
            }
            else {
                Snackbar.make(
                    (requireActivity() as MainActivity).viewPager,
                    "NO CONNECTION",
                    Snackbar.LENGTH_SHORT
                ).show()
            }

            adapter.fragments = newFrags
            frags = newFrags
            adapter.notifyDataSetChanged()
        }
    }

    inner class NpaGridLayoutManager : GridLayoutManager {
        /**
         * Disable predictive animations. There is a bug in RecyclerView which causes views that
         * are being reloaded to pull invalid ViewHolders from the internal recycler stack if the
         * adapter size has decreased since the ViewHolder was recycled.
         */

        override fun supportsPredictiveItemAnimations(): Boolean {
            return false
        }

        constructor(
            context: Context?,
            attrs: AttributeSet?,
            defStyleAttr: Int,
            defStyleRes: Int
        ) : super(context, attrs, defStyleAttr, defStyleRes)

        constructor(context: Context?, spanCount: Int) : super(context, spanCount)

        constructor(
            context: Context?,
            spanCount: Int,
            orientation: Int,
            reverseLayout: Boolean
        ) : super(context, spanCount, orientation, reverseLayout)
    }
}