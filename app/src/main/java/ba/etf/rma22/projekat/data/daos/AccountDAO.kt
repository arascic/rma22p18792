package ba.etf.rma22.projekat.data.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import ba.etf.rma22.projekat.data.models.Account

@Dao
interface AccountDAO {
    @Query("delete from Account")
    fun drop()

    @Query("UPDATE Account SET acHash= :payload WHERE 1=1")
    suspend fun postaviHash(payload: String)

    @Insert(onConflict = REPLACE)
    suspend fun insertAccount(acc: Account)

    @Query("select count(*) from Account where acHash = :hash")
    suspend fun hasAcc(hash: String): Int
}