package ba.etf.rma22.projekat.viewmodel

import androidx.lifecycle.ViewModel
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.Grupa
import ba.etf.rma22.projekat.data.models.Istrazivanje
import ba.etf.rma22.projekat.data.repositories.AnketaRepository
import ba.etf.rma22.projekat.data.repositories.IstrazivanjeIGrupaRepository
import ba.etf.rma22.projekat.retrofit.ApiAdapter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class IstrazivanjaViewModel : ViewModel() {
    companion object {
        fun getIstrazivanja(
            onSuccess: (List<Istrazivanje>) -> Unit,
            onError: () -> Unit
        ) {
            GlobalScope.launch {
                val result = IstrazivanjeIGrupaRepository.getIstrazivanja()
                when (result) {
                    is List<Istrazivanje> -> onSuccess.invoke(result)
                    else -> onError.invoke()
                }
            }
        }

        fun getGrupe(
            onSuccess: (List<Grupa>) -> Unit,
            onError: () -> Unit
        ) {
            GlobalScope.launch {
                val result = IstrazivanjeIGrupaRepository.getGrupe()
                when (result) {
                    is List<Grupa> -> onSuccess.invoke(result)
                    else -> onError.invoke()
                }
            }
        }

        fun getGrupeZaIstrazivaje(
            onSuccess: (List<Grupa>) -> Unit,
            onError: () -> Unit,
            idIstrazivanja: Int
        ) {
            GlobalScope.launch {
                val result = IstrazivanjeIGrupaRepository.getGrupeZaIstrazivanje(idIstrazivanja)
                when (result) {
                    is List<Grupa> -> onSuccess.invoke(result)
                    else -> onError.invoke()
                }
            }
        }

        fun upisiUGrupu(
            onSuccess: () -> Unit,
            onError: () -> Unit,
            idGrupa: Int
        ) {
            GlobalScope.launch {
                val result = IstrazivanjeIGrupaRepository.upisiUGrupu(idGrupa)
                if (result!!) {
                    onSuccess.invoke()
                } else {
                    onError.invoke()
                }
            }
        }

        fun getUpisaneGrupe(
            onSuccess: (List<Grupa>) -> Unit,
            onError: () -> Unit
        ) {
            GlobalScope.launch {
                val result = IstrazivanjeIGrupaRepository.getUpisaneGrupe()
                when (result) {
                    is List<Grupa> -> onSuccess.invoke(result)
                    else -> onError.invoke()
                }
            }
        }
    }
}