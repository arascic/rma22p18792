package ba.etf.rma22.projekat.data.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class OdgovorRequest(
    @SerializedName("odgovor") val odgovor: Int,
    @SerializedName("pitanje") val pid: Int,
    @SerializedName("progres") val progres: Int
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(odgovor)
        parcel.writeInt(pid)
        parcel.writeInt(progres)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<OdgovorRequest> {
        override fun createFromParcel(parcel: Parcel): OdgovorRequest {
            return OdgovorRequest(parcel)
        }

        override fun newArray(size: Int): Array<OdgovorRequest?> {
            return arrayOfNulls(size)
        }
    }
}