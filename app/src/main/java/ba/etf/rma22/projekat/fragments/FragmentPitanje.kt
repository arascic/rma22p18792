package ba.etf.rma22.projekat.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.text.SpannableString
import android.text.style.UnderlineSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import ba.etf.rma22.projekat.MainActivity
import ba.etf.rma22.projekat.data.models.Anketa
import ba.etf.rma22.projekat.data.models.AnketaState
import ba.etf.rma22.projekat.data.models.AnketaTaken
import ba.etf.rma22.projekat.data.models.Pitanje
import ba.etf.rma22.projekat.data.repositories.PitanjeAnketaRepository
import ba.etf.rma22.projekat.viewmodel.AnketaViewModel
import ba.etf.rma22.projekat.viewmodel.ViewPagerAdapter
import ba.etf.rma22.projekat.R
import ba.etf.rma22.projekat.retrofit.Connection
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.runBlocking
import java.lang.Math.abs
import java.time.Instant.now
import kotlin.math.roundToInt
import kotlin.random.Random

class FragmentPitanje(
    val pitanje: Pitanje,
    val anketa: Anketa
) : Fragment() {
    lateinit var topView: View
    lateinit var tekstPitanja: TextView
    lateinit var dugmeZaustavi: Button
    lateinit var odgovoriLista: ListView
    var selekcija: Int = anketa.responses!!.get(pitanje.id) ?: -1

    companion object {
        lateinit var anketaTaken: AnketaTaken
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        topView = inflater.inflate(R.layout.fragment_pitanje, container, false)

        tekstPitanja = topView.findViewById(R.id.tekstPitanja)
        dugmeZaustavi = topView.findViewById(R.id.dugmeZaustavi)
        odgovoriLista = topView.findViewById(R.id.odgovoriLista)

        tekstPitanja.text = pitanje.tekstPitanja

        val adapter = CustomLVAdapter(
            requireActivity(),
            android.R.layout.simple_list_item_1,
            pitanje.opcije
        )

        odgovoriLista.adapter = adapter

        odgovoriLista.setOnItemClickListener {
            parent, view, position, id -> kotlin.run {
                runBlocking {
                    if (selekcija == position) {
                        selekcija = -1
                        anketa.responses!!.remove(pitanje.id)
                        anketa.progres = anketa.responses!!.size.toFloat() /
                                PitanjeAnketaRepository.getPitanja(anketa.id)!!.size
                    } else {
                        anketa.responses!!.put(pitanje.id, position)

                        if (selekcija == -1) {
                            anketa.progres = anketa.responses!!.size.toFloat() /
                                    PitanjeAnketaRepository.getPitanja(anketa.id)!!.size
                        } else anketa.progres = anketa.progres / 100

                        selekcija = position
                    }

                    anketa.progres = anketa.progres * 100

                    odgovoriLista.adapter = null

                    // forsiranje ponovnog iscrtavanja listView-a
                    odgovoriLista.adapter = adapter
                }
            }
        }

        if(AnketaState.getState(anketa) != AnketaState.ACTIVE_NOT_DONE) {
            val brPitanja: Int
            runBlocking { brPitanja = PitanjeAnketaRepository.
                getPitanja(anketa.id)!!.size }

            val brPotrebnihOdg = (brPitanja * anketa.progres / 100).roundToInt()

            if( anketa.responses!!.size < brPotrebnihOdg &&
                AnketaState.getState(anketa) != AnketaState.EXPIRED_NOT_DONE
            ) {
                /* data je mockana kao urađena ali nismo mockali odgovore
                        pa će se to uraditi ovdje runtime */

                val response = abs(Random(now().epochSecond).nextInt()) % pitanje.opcije.size
                selekcija = response
                odgovoriLista.setSelection(response)
                anketa.responses!!.put(pitanje.id, response)
            }

            odgovoriLista.isEnabled = false
        }

        dugmeZaustavi.setOnClickListener {
            val viewPagerAdapter = ((requireActivity() as MainActivity).viewPager.adapter
                    as ViewPagerAdapter)

            if(Connection.isInternetAvailable(MainActivity.context) &&
                anketa.responses!!.isNotEmpty()
            ) {
                AnketaViewModel.posaljiOdgovore(
                    {
                        val newFrags = mutableListOf<Fragment>()
                        newFrags.add(0, FragmentAnkete())
                        newFrags.add(1, FragmentIstrazivanje())

                        viewPagerAdapter.fragments = newFrags
                        MainActivity.frags = newFrags
                        (requireActivity() as MainActivity).runOnUiThread {
                            (requireActivity() as MainActivity).viewPager.currentItem = 0
                            viewPagerAdapter.notifyDataSetChanged()
                        }

                        anketa.anketaTaken = anketaTaken.id
                    },
                    {},
                    anketaTaken.id,
                    anketa.responses!!
                )
            }
            else {
                val newFrags = mutableListOf<Fragment>()
                newFrags.add(0, FragmentAnkete())
                newFrags.add(1, FragmentIstrazivanje())

                viewPagerAdapter.fragments = newFrags
                MainActivity.frags = newFrags
                (requireActivity() as MainActivity).viewPager.currentItem = 0
                (requireActivity() as MainActivity).runOnUiThread {
                    viewPagerAdapter.notifyDataSetChanged()
                }

                if(Connection.isInternetAvailable(MainActivity.context))
                    anketa.anketaTaken = anketaTaken.id
            }
        }

        if(!Connection.isInternetAvailable(MainActivity.context)) {
            Snackbar.make(
                (requireActivity() as MainActivity).viewPager,
                "NO CONNECTION",
                Snackbar.LENGTH_SHORT
            ).show()
            odgovoriLista.isEnabled = false
        }

        return topView
    }

    inner class CustomLVAdapter(context: Context, layout: Int, options: List<String>)
        : ArrayAdapter<String>(context, layout, options) {

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            val pitanje: String = getItem(position)!!
            lateinit var retView: TextView

            if(convertView == null) {
                retView = LayoutInflater.from(getContext()).
                    inflate(android.R.layout.simple_list_item_1, parent, false)
                    as TextView
            }
            else retView = convertView as TextView

            retView.text = pitanje
            if(position == selekcija) {
                retView.setTextColor(Color.parseColor("#0000FF"))

                val content = SpannableString(retView.text)
                content.setSpan(UnderlineSpan(), 0, content.length, 0)
                retView.text = content
            }
            else retView.setTextColor(Color.BLACK)

            return retView
        }
    }
}