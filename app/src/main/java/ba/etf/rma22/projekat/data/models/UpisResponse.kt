package ba.etf.rma22.projekat.data.models

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class UpisResponse(
    @SerializedName("message") val value: String
) : Parcelable {

    constructor(parcel: Parcel) : this(parcel.readString()!!)

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(value)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UpisResponse> {
        override fun createFromParcel(parcel: Parcel): UpisResponse {
            return UpisResponse(parcel)
        }

        override fun newArray(size: Int): Array<UpisResponse?> {
            return arrayOfNulls(size)
        }
    }
}