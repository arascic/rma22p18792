package ba.etf.rma22.projekat.data.models

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class AnketaTaken(
    @PrimaryKey(autoGenerate = true) @SerializedName("id") var id: Int,
    @ColumnInfo(name = "progres") @SerializedName("progres") var progres: Int,
    @ColumnInfo(name = "datumRada") @SerializedName("datumRada") var datumRada: String?,
    @ColumnInfo(name = "student") @SerializedName("student") var student: String?,
    @ColumnInfo(name = "kreirano") @SerializedName("createdAt") var kreirano: String?,
    @ColumnInfo(name = "promijenjeno") @SerializedName("updatedAt") var promijenjeno: String?,
    @ColumnInfo(name = "AnketumId") @SerializedName("AnketumId") var AnketumId: Int
): Parcelable {

    var AnketaId: Int? = -1

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readInt()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeInt(progres)
        parcel.writeString(datumRada)
        parcel.writeString(student)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AnketaTaken> {
        override fun createFromParcel(parcel: Parcel): AnketaTaken {
            return AnketaTaken(parcel)
        }

        override fun newArray(size: Int): Array<AnketaTaken?> {
            return arrayOfNulls(size)
        }
    }
}