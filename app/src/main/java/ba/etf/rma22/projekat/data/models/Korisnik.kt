package ba.etf.rma22.projekat.data.models

import android.util.Log
import ba.etf.rma22.projekat.data.repositories.*
import ba.etf.rma22.projekat.retrofit.ApiAdapter
import kotlinx.coroutines.*
import kotlin.coroutines.suspendCoroutine

class Korisnik {
    companion object {
        private var upisanDef: Deferred<List<Grupa>>
        var upisan: MutableList<Grupa>

        init {
            runBlocking {
                upisanDef = GlobalScope.async(Dispatchers.Default) { IstrazivanjeIGrupaRepository.getUpisaneGrupe()!! }
                upisanDef.await()
                upisan = upisanDef.getCompleted().toMutableList()

                val istrazivanja = GlobalScope.async(Dispatchers.Default)
                    { IstrazivanjeIGrupaRepository.getIstrazivanja() }
                istrazivanja.await()

                upisan.forEach { it1 ->
                    var temp = IstrazivanjeIGrupaRepository
                        .getGrupeZaIstrazivanje(it1.istrazivanjeID)!!
                    if(it1 in temp) {
                        Log.d("DEBUG DEBUG", IstrazivanjeRepository.istrazivanja.toString())

                        it1.nazivIstraživanja = IstrazivanjeRepository.istrazivanja
                            .find { it.id.equals(it1.istrazivanjeID) }!!.naziv
                    }
                }
            }
        }

        fun getUpisani(): List<Grupa> {
            return upisan
        }
    }
}